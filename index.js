

/** Bài 1: Tính tiền Lương nân viên
 * Đầu vào:
 * + Số ngày làm của nhân viên : 5
 * + Lương 1 ngày của nhân viên : 100.000
 * 
 * Các bước xử lí:
 * Bước 1: Tạo biến ngày làm, tiền lương là: soNgay, tienLuong
 * Bước 2: Gán giá trị cho soNgay = 5, tienLuong = 100000
 * Bước 3: Viết chương trình tính tiền lương
 * 
 * 
 * Đầu ra: 
 * 
 * In ra kết quả console.log()
 * 
 */
var soNgay = 5;

var tienLuong = 100000;
var tienLuong = tienLuong * soNgay;

console.log("Tiền lương của bạn là:" + tienLuong + "VND")





/** Bài 2: Tính giá trị trung bình
 * 
 * Đầu vào: 
 * + cho 5 số thực: 1, 3, 5, 6, 7
 * 
 * Các bước sử lý:
 * Bước 1: Gán biến cho 5 giá tri
 * Bước 2: Viết chương trình tính giá trị trung bình
 * 
 * Đầu ra:
 * In ra kết quả Console.log(giaTriTrungBinh)
 * 
 * 
*/
var num1 = 10;
var num2 = 3;
var num3 = 5;
var num4 = 6;
var num5 = 7;

var giaTriTrungBinh = (num1 + num2 + num3 + num4 + num5) / 5;

console.log("Gía trị trung bình là:",giaTriTrungBinh);





/** Bài 3: Quy đổi tiền
 * 
 * Đầu vào:
 * + 23.500 vnd
 * + 2 usd
 * 
 * Các bước xử lý:
 * 
 * Bước 1: Gán biến var vnd = 23500; gán biến var usd = 2;
 * Bước 2: Viết chương trình: var money = usd * vnd
 * 
 * Đầu ra:
 * In ra kết quả console.log()
 * 
*/
var usd = 2;
var vnd = 23500;
var money = usd * vnd;
console.log("Số tiền sau quy đổi =",money + "VND")





/** Bài 4: Tính diện tích và chu vi hình chữ nhật
 * 
 * Đầu vào:
 * Chiều dài: 30cm
 * chiều rộng: 20cm
 * 
 * Các bước xử lý:
 * Bước 1: gán biến: var chieuDai = 30cm, var chieuRong = 20cm;
 * Bước 2: Viết công thức tính: 
 *   var dienTich = chieuDai * chieRong
 *   var chuVi = (chieuDai + chieuRong)*2
 * 
 * Đầu ra
 * console.log(dienTich)
 * console.log(chuVi)
 */

    var chieuDai = 30;
    var chieuRong = 20;

    var dienTich = chieuDai * chieuRong;
    var chuVi = (chieuDai + chieuRong)*2;

    console.log("Diện tích hình chữ nhật là:",dienTich)
    console.log("Chu vi hình chữ nhật là:", chuVi)


    /** Bài 5: Tính tổng 2 ký Số
     * 
     * Đầu vào: 
     * number = 83;
     * 
     * Các bước xử lý
     * Bước 1: Gán biến var number = 83;
     * Bước 2: tách số hàng đơn vị: 
     *      var donVi = number % 10
     *      var hangChuc = number / 10
     * 
     * Bước 3: Viết cong thức tính tổng 2 ký Số
     * var result = donVi + hangChuc
     * 
     * Đầu ra:
     * console.log(result)
    */

        var number = 83;

        var donVi = number % 10;
        var hangChuc = Math.floor(number / 10);
        var result = donVi + hangChuc

        console.log("Tổng 2 ký số là:",result)